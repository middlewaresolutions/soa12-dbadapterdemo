# README #

Ce fichier vous permet d'installer ce projet de démonstration.

### What is this repository for? ###

Ce projet illustre la capacité à réaliser une composition de composites SCA avec la SOA Suite 12.1 d'Oracle.
Chaque composite réalise des opérations sur une notion principale TableA et des notions annexes ATableB et MasterOfA.
Les détails de la mise en oeuvre sont explicités sur mon [Blog Middleware Solutions](http://www.middleware-solutions.fr)

### Installation ###

* Installer une version de développement Oracle SOA Suite
http://www.oracle.com/technetwork/middleware/soasuite/downloads/index.html

* Utiliser une base de données.
Cette démonstration utilise Oracle XE 11r2.
http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html

* La version opensource de SoapUI.
https://www.soapui.org/downloads/soapui.html

### Exécution du projet ###

Depuis JDev:
* exécuter le serveur IntegratedServer

Depuis la console Weblogic:
* Ajouter une datasource vers la DB
* Configurer l'application DBAdapter

Depuis JDev:
* Déployer les applications depuis JDevelopper

Depuis SoapUI:
* Lancer les cas de tests